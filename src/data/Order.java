/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class Order implements Serializable{
    private String orderCode;
    private Date orderDate;
    private String type;     // import  //export
    private HashMap<String,Product> mapProduct;

    public Order(String orderCode, Date orderDate, String type, HashMap<String, Product> mapProduct) {
        this.orderCode = orderCode;
        this.orderDate = orderDate;
        this.type = type;
        this.mapProduct = mapProduct;
    }

    
    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HashMap<String, Product> getMapProduct() {
        return mapProduct;
    }

    public void setMapProduct(HashMap<String, Product> mapProduct) {
        this.mapProduct = mapProduct;
    }

    
   
    
}
