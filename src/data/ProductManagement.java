
package data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import utils.Validation;


public class ProductManagement {

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private ArrayList<Product> listProduct = new ArrayList();
    private ArrayList<Order> listOrder = new ArrayList();

    public void addAProduct() {
        String code, confirm;
        int index;
        do {
            do {
                code = Validation.regexString("Enter Code(Pxxx): ", "Wrong.Input again!", "^[P|p]\\d{3}$");
                index = getIndexByID(code);
                if (index != -1) {
                    System.out.println("Duplicated ID.Input again!");
                }
            } while (index != -1);
            String name = Validation.getString("Enter Name: ", "Wrong.Input again!");
            int choice = Validation.getAnInteger("(1.Daily Product or 2.Long Product): ", "Just 1 or 2", 1, 2);
            if (choice == 1) {
                String unit = Validation.getString("Enter Unit: ", "Wrong.Input again!");
                String size = Validation.getString("Enter Size: ", "Wrong.Input again!");
                Product dProduct = new DailyProduct(code, name, unit, size);
                listProduct.add(dProduct);
                System.out.println("Successfull.");
            } else {
                Date mDate, eDate;
                Date now = new Date();
                
                do {
                    mDate = Validation.getDate("Enter Manufactoring Date(dd/MM/yyyy): ", "Wrong.Input again!");
                    if (mDate.after(now)) {
                        System.out.println("Manufactoring date must be less than or equal to the Current date.");
                    }
                } while (mDate.after(now));
                do {
                    eDate = Validation.getDate("Enter Expired Date(dd/MM/yyyy): ", "Wrong.Input again!");
                    if (eDate.before(mDate)) {
                        System.out.println("The Expired Date must be greater than or equal to the Manufactoring Date.");
                    }
                } while (eDate.before(mDate));
                Product lProduct = new LongProduct(code, name, mDate, eDate);
                listProduct.add(lProduct);
                System.out.println("Successfull.");
            }
            confirm = Validation.regexString("Do you want to add continues(y/n): ", "Just y or n", "^[y|Y|n|N]$");
        } while (confirm.equalsIgnoreCase("y"));

    }

    public int getIndexByID(String id) {
        for (int i = 0; i < listProduct.size(); i++) {
            if (listProduct.get(i).getCode().equalsIgnoreCase(id)) {
                return i;
            }
        }
        return -1;
    }

    public void updateAProduct() {
        String code = Validation.regexString("Enter Product ID(Pxxx): ", "Wrong.Input again!", "^[P|p]\\d{3}$");
        int index = getIndexByID(code);
        if (index == -1) {
            System.out.println("Product does not exist");
        } else {
            String newName = Validation.updateString("Enter New Name: ", listProduct.get(index).getName());
            if (listProduct.get(index) instanceof DailyProduct) {
                String newUnit = Validation.updateString("Enter New Unit: ", ((DailyProduct) listProduct.get(index)).getUnit());
                String newSize = Validation.updateString("Enter New Size: ", ((DailyProduct) listProduct.get(index)).getSize());
                ((DailyProduct) listProduct.get(index)).setName(newName);
                ((DailyProduct) listProduct.get(index)).setUnit(newUnit);
                ((DailyProduct) listProduct.get(index)).setSize(newSize);
                System.out.println("Successfull");
            } else {
                ((LongProduct) listProduct.get(index)).setName(newName);
                String confirmChangeManufactoring = Validation.regexString("Do you want to change Manufactoring Date(y/n):", "Just y or n", "^[Y|y|n|N]$");
                if (confirmChangeManufactoring.equalsIgnoreCase("Y")) {
                    Date now = new Date();
                    Date manufactoringDate;
                    do {
                        manufactoringDate = Validation.getDate("Enter Manufactoring Date(dd/MM/yyyy): ", "Wrong.Input again!");
                        if (manufactoringDate.after(now)) {
                            System.out.println("Manufactoring date must be less than or equal to the Current date.");
                        }
                    } while (manufactoringDate.after(now));
                    ((LongProduct) listProduct.get(index)).setmDate(manufactoringDate);
                }
                String confirmChangeExpired = Validation.regexString("Do you want to change Expired Date(y/n):", "Just y or n", "^[Y|y|n|N]$");
                if (confirmChangeExpired.equalsIgnoreCase("Y")) {
                    Date expiredDate;
                    do {
                        expiredDate = Validation.getDate("Enter Expired Date(dd/MM/yyyy): ", "Wrong.Input again!");
                        if (expiredDate.before(((LongProduct) listProduct.get(index)).getmDate())) {
                            System.out.println("The Expiration date must be greater than or equal to the Manufacturing date.");
                        }
                    } while (expiredDate.before(((LongProduct) listProduct.get(index)).getmDate()));
                    ((LongProduct) listProduct.get(index)).seteDate(expiredDate);
                }
                System.out.println("Update Successfull.");
            }
        }
    }

    public void deleteAProduct() {
        String code = Validation.regexString("Enter Product ID(Pxxx): ", "Wrong.Input again!", "^[P|p]\\d{3}$");
        int index = getIndexByID(code);
        if (index == -1) {
            System.out.println("Product does not exist");
        } else {
            boolean check = false;
            for (Order o : listOrder) {
                for (Product p : o.getMapProduct().values()) {
                    if (p.getCode().equalsIgnoreCase(code)) {
                        check = true;
                    }
                }
            }
            if (check == true) {
                System.out.println("This product already exists in the import/export receipt. Fail");
            } else {
                String confirm = Validation.regexString("Are you sure delete(Y/N): ", "Just y or n", "^[Y|y|N|n]$");
                if (confirm.equalsIgnoreCase("Y")) {
                    listProduct.remove(index);
                    System.out.println("Delete Successfull.");
                } else {
                    System.out.println("You cancel. Fail.");
                }
            }
        }
    }

    public void showAllProduct() {
        if (listProduct.isEmpty()) {
            System.out.println("List Product is empty.Nothing to print.");
        } else {
            System.out.println("HERE IS PRODUCT'S INFORMATION:");
            System.out.println("DAILY PRODUCT:");
            System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
            System.out.println("|CODE |        NAME        | QUANTITY |          UNIT      |         SIZE       |");
            System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
            for (int i = 0; i < listProduct.size(); i++) {
                if (listProduct.get(i) instanceof DailyProduct) {
                    System.out.printf("|%-5s|%-20s|%-10d|%-20s|%-20s|\n", listProduct.get(i).getCode(), listProduct.get(i).getName(),
                            listProduct.get(i).getQuantity(), ((DailyProduct) listProduct.get(i)).getUnit(), ((DailyProduct) listProduct.get(i)).getSize());
                }
            }
            System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
            System.out.println("LONG PRODUCT:");
            System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
            System.out.println("|CODE |        NAME        | QUANTITY | MANUFACTORING DATE |     EXPIRED DATE   |");
            System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
            for (int i = 0; i < listProduct.size(); i++) {
                if (listProduct.get(i) instanceof LongProduct) {
                    System.out.printf("|%-5s|%-20s|%-10d|%-20s|%-20s|\n", listProduct.get(i).getCode(), listProduct.get(i).getName(),
                            listProduct.get(i).getQuantity(), sdf.format(((LongProduct) listProduct.get(i)).getmDate()), sdf.format(((LongProduct) listProduct.get(i)).geteDate()));
                }
            }
            System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
        }
    }

    public void createOrder() {
        String oCode, confirm;
        boolean checkExist;
        do {
            oCode = Validation.regexString("Enter Order Code(Ox..)(6digit): ", "Wrong.Input again!", "^[o|O]\\d{6}$");
            checkExist = checkIDOrder(oCode);
            if (checkExist) {
                System.out.println("Duplicated ID.Input again!");
            }
        } while (checkExist);
        Date now = new Date();
        String type;
        int choice = Validation.getAnInteger("(1.Import Code 2.Export Code): ", "Just 1 or 2.", 1, 2);
        HashMap<String, Product> mapOrderProduct = new HashMap();
         
        if (choice == 1) {
            type = "Import Receipt";
            do {
                int stt = 1;
                System.out.println("+-----+-----+--------------------+----------+-------------------------+-------------------------+");
                System.out.println("| STT |CODE |        NAME        | QUANTITY | MANUFACTORING DATE/UINT |   EXPIRATION DATE/SIZE  |");
                System.out.println("+-----+-----+--------------------+----------+-------------------------+-------------------------+");
                for (int i = 0; i < listProduct.size(); i++) {
                    if (listProduct.get(i) instanceof DailyProduct) {
                        System.out.printf("|%-5d|%-5s|%-20s|%-10d|%-25s|%-25s|\n", stt++, listProduct.get(i).getCode(), listProduct.get(i).getName(),
                                listProduct.get(i).getQuantity(), ((DailyProduct) listProduct.get(i)).getUnit(), ((DailyProduct) listProduct.get(i)).getSize());
                    } else {
                        System.out.printf("|%-5d|%-5s|%-20s|%-10d|%-25s|%-25s|\n", stt++, listProduct.get(i).getCode(), listProduct.get(i).getName(),
                                listProduct.get(i).getQuantity(), sdf.format(((LongProduct) listProduct.get(i)).getmDate()), sdf.format(((LongProduct) listProduct.get(i)).geteDate()));
                    }
                }
                System.out.println("+-----+-----+--------------------+----------+-------------------------+-------------------------+");
                int choiceProduct = Validation.getAnInteger("Enter choice[1.." + listProduct.size() + "]: ", "Input again!", 1, listProduct.size());
                int quantity = Validation.getAnInteger("Enter quantity: ", "More than 0.Input again!", 0);
                if (listProduct.get(choiceProduct - 1) instanceof LongProduct) {
                    if (((LongProduct) listProduct.get(choiceProduct - 1)).geteDate().before(now)) {
                        System.out.println("The product has expired.");
                    } else {
                        mapOrderProduct.put(listProduct.get(choiceProduct - 1).getCode(), (LongProduct) listProduct.get(choiceProduct - 1));
                        listProduct.get(choiceProduct - 1).setQuantity(listProduct.get(choiceProduct - 1).getQuantity() + quantity);
                        System.out.println("A product has been entered in the receipt.");
                    }
                } else {
                    mapOrderProduct.put(listProduct.get(choiceProduct - 1).getCode(), (DailyProduct) listProduct.get(choiceProduct - 1));
                    listProduct.get(choiceProduct - 1).setQuantity(listProduct.get(choiceProduct - 1).getQuantity() + quantity);
                    System.out.println("A product has been entered in the receipt.");
                }
                confirm = Validation.regexString("Do you want to import more products(y/n):", "Just y or n", "^[Y|y|N|n]$");
            } while (confirm.equalsIgnoreCase("y"));
            listOrder.add(new Order(oCode, now, type, mapOrderProduct));
            System.out.println("Import successfull");
        } else {
            type = "Export Receipt";
            do {
                int stt = 1;
                System.out.println("+-----+-----+--------------------+----------+-------------------------+-------------------------+");
                System.out.println("| STT |CODE |        NAME        | QUANTITY | MANUFACTORING DATE/UINT |   EXPIRATION DATE/SIZE  |");
                System.out.println("+-----+-----+--------------------+----------+-------------------------+-------------------------+");
                for (int i = 0; i < listProduct.size(); i++) {
                    if (listProduct.get(i) instanceof DailyProduct) {
                        System.out.printf("|%-5d|%-5s|%-20s|%-10d|%-25s|%-25s|\n", stt++, listProduct.get(i).getCode(), listProduct.get(i).getName(),
                                listProduct.get(i).getQuantity(), ((DailyProduct) listProduct.get(i)).getUnit(), ((DailyProduct) listProduct.get(i)).getSize());
                    } else {
                        System.out.printf("|%-5d|%-5s|%-20s|%-10d|%-25s|%-25s|\n", stt++, listProduct.get(i).getCode(), listProduct.get(i).getName(),
                                listProduct.get(i).getQuantity(), sdf.format(((LongProduct) listProduct.get(i)).getmDate()), sdf.format(((LongProduct) listProduct.get(i)).geteDate()));
                    }
                }
                System.out.println("+-----+-----+--------------------+----------+-------------------------+-------------------------+");
                int choiceProduct = Validation.getAnInteger("Enter choice[1.." + listProduct.size() + "]: ", "Input again!", 1, listProduct.size());
                int quantity = Validation.getAnInteger("Enter quantity: ", "More than 0.Input again!", 0);
                if (quantity > listProduct.get(choiceProduct - 1).getQuantity()) {
                    System.out.println("The output quantity of the product is greater than the quantity in stock.");
                } else {
                    listProduct.get(choiceProduct - 1).setQuantity(listProduct.get(choiceProduct - 1).getQuantity() - quantity);
                    mapOrderProduct.put(listProduct.get(choiceProduct - 1).getCode(), listProduct.get(choiceProduct - 1));
                    System.out.println("A product has been issued in the receipt.");
                }
                confirm = Validation.regexString("Do you want to import more products(y/n):", "Just y or n", "^[Y|y|N|n]$");
            } while (confirm.equalsIgnoreCase("y"));
            listOrder.add(new Order(oCode, now, type, mapOrderProduct));
            System.out.println("Receipt successfull");
        }
    }

    public boolean checkIDOrder(String id) {
        boolean check = false;
        for (int i = 0; i < listOrder.size(); i++) {
            if (listOrder.get(i).getOrderCode().equalsIgnoreCase(id)) {
                check = true;
            }
        }
        return check;
    }

    public void productExpired() {
        boolean check = false;
        Date now = new Date();
        System.out.println("HERE IS LIST PRODUCT THAT HAVE EXPIRED");
        System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
        System.out.println("|CODE |        NAME        | QUANTITY | MANUFACTORING DATE |   EXPIRATION DATE  |");
        System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
        for (int i = 0; i < listProduct.size(); i++) {
            if (listProduct.get(i) instanceof LongProduct) {
                if (now.after(((LongProduct) listProduct.get(i)).geteDate())) {
                    System.out.printf("|%-5s|%-20s|%-10d|%-20s|%-20s|\n", listProduct.get(i).getCode(),
                            listProduct.get(i).getName(), listProduct.get(i).getQuantity(),
                            sdf.format(((LongProduct) listProduct.get(i)).getmDate()), sdf.format(((LongProduct) listProduct.get(i)).geteDate()));
                    check = true;
                }
            }
        }
        System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
        if (check == false) {
            System.out.println("There are no expired products.");
        }
    }

    public void productISSelling() {
        boolean check = false;
        Date now = new Date();
        System.out.println("HERE IS LIST PRODUCT THAT STORE IS SELLING");
        System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
        System.out.println("|CODE |        NAME        | QUANTITY | MANUFACTORING DATE |   EXPIRATION DATE  |");
        System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
        for (int i = 0; i < listProduct.size(); i++) {
            if (listProduct.get(i) instanceof LongProduct) {
                if (listProduct.get(i).getQuantity() > 0 && now.before(((LongProduct) listProduct.get(i)).geteDate())) {
                    System.out.printf("|%-5s|%-20s|%-10d|%-20s|%-20s|\n", listProduct.get(i).getCode(),
                            listProduct.get(i).getName(), listProduct.get(i).getQuantity(),
                            sdf.format(((LongProduct) listProduct.get(i)).getmDate()), sdf.format(((LongProduct) listProduct.get(i)).geteDate()));
                    check = true;
                }
            }
        }
        System.out.println("+-----+--------------------+----------+--------------------+--------------------+");
        if (check == false) {
            System.out.println("There are no products for sale.");
        }
    }

    public void productOutOfStock() {
        Collections.sort(listProduct);
        boolean check = false;
        System.out.println("HERE IS LIST PRODUCT THAT ARE RUNNING OUT OF STOCK");
        System.out.println("+-----+--------------------+----------+-------------------------+-------------------------+");
        System.out.println("|CODE |        NAME        | QUANTITY | MANUFACTORING DATE/UINT |   EXPIRATION DATE/SIZE  |");
        System.out.println("+-----+--------------------+----------+-------------------------+-------------------------+");
        for (int i = 0; i < listProduct.size(); i++) {
            if (listProduct.get(i).getQuantity() <= 3) {
                if (listProduct.get(i) instanceof DailyProduct) {
                    System.out.printf("|%-5s|%-20s|%-10d|%-25s|%-25s|\n", listProduct.get(i).getCode(), listProduct.get(i).getName(),
                            listProduct.get(i).getQuantity(), ((DailyProduct) listProduct.get(i)).getUnit(), ((DailyProduct) listProduct.get(i)).getSize());
                } else {
                    System.out.printf("|%-5s|%-20s|%-10d|%-25s|%-25s|\n", listProduct.get(i).getCode(), listProduct.get(i).getName(),
                            listProduct.get(i).getQuantity(), sdf.format(((LongProduct) listProduct.get(i)).getmDate()), sdf.format(((LongProduct) listProduct.get(i)).geteDate()));
                }
                check = true;
            }
        }
        System.out.println("+-----+--------------------+----------+-------------------------+-------------------------+");
        if(check == false){
            System.out.println("There is no product smaller than three");
    }
}
public void checkProductInReceipt() {
        String code = Validation.regexString("Enter Product ID(Pxxx): ", "Wrong.Input again!", "^[P|p]\\d{3}$");
        boolean check = false;
        for (Order o : listOrder) {
            for (Product p : o.getMapProduct().values()) {
                if (code.equalsIgnoreCase(p.getCode())) {
                    System.out.println("Products available in receipt:" + o.getOrderCode());
                    check = true;
                }
            }
        }
        if (check == false) {
            System.out.println("The product is not included in the import/export receipt.");
        }
    }

    public void saveData() {
        boolean checkProduct = true, checkWareHouse = true;
        try {
            FileOutputStream fos = new FileOutputStream("product.dat");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(listProduct);
            oos.close();
            fos.close();
        } catch (IOException e) {
            System.out.println("Save data to product.dat fail!");
            checkProduct = false;
        }
        if (checkProduct) {
            System.out.println("Save data to product.dat successfully!");
        }
        try {
            FileOutputStream fos = new FileOutputStream("wareHouse.dat");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(listOrder);
            oos.close();
            fos.close();
        } catch (IOException e) {
            System.out.println("Save data to wareHouse.dat fail!");
            checkWareHouse = false;
        }
        if (checkWareHouse) {
            System.out.println("Save data to wareHouse.dat successfully!");
        }
    }

    public void loadData() {
        boolean checkProduct = true, checkWareHouse = true;
        try {
            FileInputStream fis = new FileInputStream("product.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            listProduct =  (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Load data from product.dat fail!");
            checkProduct = false;
        }
        if (checkProduct) {
            System.out.println("Load data from product.dat successfully!");
        }
        try {
            FileInputStream fis = new FileInputStream("wareHouse.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            listOrder = (ArrayList<Order>) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Load data from wareHouse.dat fail!");
            checkWareHouse = false;
        }
        if (checkWareHouse) {
            System.out.println("Load data from wareHouse.dat successfully!");
        }
    }
}
