
package data;

import java.io.Serializable;

public class DailyProduct extends Product implements Serializable{
    private String unit;
    private String size;

    public DailyProduct(String code, String name,String unit, String size) {
        super(code, name);
        this.unit = unit;
        this.size = size;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
    
    
}
