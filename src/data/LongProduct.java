
package data;

import java.io.Serializable;
import java.util.Date;
public class LongProduct extends Product implements Serializable{
    private Date mDate;
    private Date eDate;

    public LongProduct( String code, String name,Date mDate, Date eDate) {
        super(code, name);
        this.mDate = mDate;
        this.eDate = eDate;
    }

    public Date getmDate() {
        return mDate;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    public Date geteDate() {
        return eDate;
    }

    public void seteDate(Date eDate) {
        this.eDate = eDate;
    }

    
    
}
