
package data;

import java.io.Serializable;

public class Product implements Serializable,Comparable<Product>{
    protected String code;
    protected String name;
    protected int quantity = 0;

    public Product() {
    }

    public Product(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int compareTo(Product o) {
        return this.getQuantity() - o.getQuantity();
    }
    

}
