/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import data.ProductManagement;
import view.Menu;


public class WareHouse {

    public static void main(String[] args) {
        ProductManagement sm = new ProductManagement();
        sm.loadData();
        Menu menu = new Menu("Store Management at Convenience Store.");
        menu.addNewOption("|               1. Manage Products.                  |");
        menu.addNewOption("|               2. Manage Warehouse.                 |");
        menu.addNewOption("|               3. Report.                           |");
        menu.addNewOption("|               4. Store date to file.               |");
        menu.addNewOption("|               5. Exit!                             |");

        Menu menuProduct = new Menu(" Product Management.                  ");     
        menuProduct.addNewOption("|           1. Add a product.                        |");
        menuProduct.addNewOption("|           2. Update product information.           |");
        menuProduct.addNewOption("|           3. Delete product.                       |");
        menuProduct.addNewOption("|           4. Show all product.                     |");
        menuProduct.addNewOption("|           5. Back to main menu.                    |");

        Menu menuWarehouse = new Menu(" Warehouse Management.                ");
        menuWarehouse.addNewOption("|           1. Create an import/export receipt.             |");
        menuWarehouse.addNewOption("|           2. Back to main menu.                    |");

        Menu menuReport = new Menu("Report Management.                    ");
        menuReport.addNewOption("|   1. Products that have expired.                   |");
        menuReport.addNewOption("|   2. The products that the store is selling.       |");
        menuReport.addNewOption("|   3. Products that are running out of stock.       |");
        menuReport.addNewOption("|   4. Import/export receipt of a product.           |");
        menuReport.addNewOption("|   5. Back to main menu.                            |");

        int choice;
        do {
            menu.printMenu();
            choice = menu.getChoice();
            switch (choice) {
                case 1:
                    int choiceProduct;
                    do {
                        menuProduct.printMenu();
                        choiceProduct = menuProduct.getChoice();
                        switch (choiceProduct) {
                            case 1:
                                sm.addAProduct();
                                break;
                            case 2:
                                sm.updateAProduct();
                                break;
                            case 3:
                                sm.deleteAProduct();
                                break;
                            case 4:
                                sm.showAllProduct();
                                break;
                            case 5:
                                System.out.println("Return main menu!");
                                break;
                        }
                    } while (choiceProduct != 5);
                    break;
                case 2:
                    int choiceWarehouse;
                    do {
                        menuWarehouse.printMenu();
                        choiceWarehouse = menuWarehouse.getChoice();
                        switch (choiceWarehouse) {
                            case 1:
                                sm.createOrder();
                                break;
                            case 2:
                                System.out.println("Return main menu!");
                                break;
                        }
                    } while (choiceWarehouse != 2);
                    break;
                case 3:
                    int choiceReport;
                    do {
                        menuReport.printMenu();
                        choiceReport = menuReport.getChoice();
                        switch (choiceReport) {
                            case 1:
                                sm.productExpired();
                                break;
                            case 2:
                                sm.productISSelling();
                                break;
                            case 3:
                                sm.productOutOfStock();
                                break;
                            case 4:
                                sm.checkProductInReceipt();
                                break;
                            case 5:
                                System.out.println("Return main menu!");
                                break;
                        }
                    } while (choiceReport != 5);
                    break;
                case 4:
                    sm.saveData();
                    break;
                case 5:
                    System.out.println("Exist!");
                    break;
            }
        } while (choice != 5);
    }
}
