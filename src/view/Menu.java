
package view;

import java.util.ArrayList;
import utils.Validation;

public class Menu {
    private String title;
    private ArrayList<String> optionList = new ArrayList();

    public Menu(String title) {
        this.title = title;
    }
        
    public void addNewOption(String newOption) { 
        optionList.add(newOption);   
    }

    public void printMenu() {
        System.out.println("+----------------------------------------------------+");
        System.out.println("|  Welcome To " + title + " |"); 
        System.out.println("+----------------------------------------------------+");
        for (int i = 0; i < optionList.size(); i++) {
            System.out.println(optionList.get(i));
        }
        System.out.println("+----------------------------------------------------+");
    }
    
    public int getChoice() {
        int maxOption = optionList.size();
        String inputMsg = "Choose [1.." + maxOption + "]: ";
        String errorMsg = "You are required to choose the option 1.." + maxOption;
        return Validation.getAnInteger(inputMsg, errorMsg, 1, maxOption);
    }
}